import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show SystemChrome, DeviceOrientation;
import 'package:intl/intl.dart' show DateFormat;

void main() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.landscapeRight,
    DeviceOrientation.landscapeLeft,
  ]);
  SystemChrome.setEnabledSystemUIOverlays([]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'GDG clock',
      theme: ThemeData(
        brightness: Brightness.dark,
        textTheme: TextTheme(
          display1: TextStyle(
            color: Colors.black38,
            fontSize: 30,
          ),
        ),
        fontFamily: 'Calibri',
      ),
      home: Scaffold(
        body: Clock(),
      ),
    );
  }
}

class Clock extends StatefulWidget {
  Clock({Key key}) : super(key: key);
  @override
  _ClockState createState() => _ClockState();
}

class _ClockState extends State<Clock> with SingleTickerProviderStateMixin {
  DateTime _now = DateTime.now();
  AnimationController animationController;

  @override
  void initState() {
    Timer.periodic(Duration(seconds: 1), (v) {
      setState(() {
        _now = DateTime.now();
      });
    });
    super.initState();

    animationController =
        AnimationController(duration: const Duration(seconds: 20), vsync: this)
          ..repeat();
  }

  Animatable<Color> bg = TweenSequence<Color>([
    TweenSequenceItem(
      weight: 1.0,
      tween: ColorTween(
        begin: Colors.black,
        end: Colors.blueAccent,
      ),
    ),
    TweenSequenceItem(
      weight: 1.0,
      tween: ColorTween(
        begin: Colors.blueAccent,
        end: Colors.black,
      ),
    ),
    TweenSequenceItem(
      weight: 1.0,
      tween: ColorTween(
        begin: Colors.black,
        end: Colors.redAccent,
      ),
    ),
    TweenSequenceItem(
      weight: 1.0,
      tween: ColorTween(
        begin: Colors.redAccent,
        end: Colors.black,
      ),
    ),
  ]);

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final hour = DateFormat('HH').format(_now);
    final minute = DateFormat('mm').format(_now);
    final second = DateFormat('ss').format(_now);

    return AnimatedBuilder(
      animation: animationController,
      builder: (_, child) {
        return Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.height / 3 * 5,
          color: bg.evaluate(AlwaysStoppedAnimation(animationController.value)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Text(
                        hour,
                        style: TextStyle(color: Colors.white, fontSize: 80),
                      )
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Opacity(
                        opacity: 1,
                        // opacity: blinkAnimation.value,
                        child: Text(
                          ' : ',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 40,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        minute,
                        style: TextStyle(color: Colors.white, fontSize: 80),
                      )
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Opacity(
                        // opacity: blinkAnimation.value,
                        opacity: 1,
                        child: Text(
                          ' : ',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 40,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        second,
                        style: TextStyle(color: Colors.white, fontSize: 80),
                      )
                    ],
                  )
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
